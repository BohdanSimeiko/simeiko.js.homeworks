const { src, dest, series, watch } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const csso = require("gulp-csso");
const include = require("gulp-file-include");
const htmlmin = require("gulp-htmlmin");
const del = require("del");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const purgecss = require("gulp-purgecss");
const minifyjs = require("gulp-js-minify");
const imagemin = require("gulp-imagemin");
const sync = require("browser-sync").create();

function html() {
  return src("src/**.html")
    .pipe(
      include({
        prefix: "@@",
      })
    )
    .pipe(
      htmlmin({
        collapseWhitespace: true,
      })
    )
    .pipe(concat("index.html"))
    .pipe(dest("dist"));
}

function scss() {
  return src("src/scss/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
        overrideBrowserlist: ["last 2 versions"],
      })
    )
    .pipe(
      purgecss({
        content: ["src/index.html"],
      })
    )
    .pipe(csso())
    .pipe(concat("style.min.css"))
    .pipe(dest("./dist"));
}

function clear() {
  return del("dist");
}

function jsmin() {
  return src("src/**.js")
    .pipe(minifyjs())
    .pipe(concat("add.min.js"))
    .pipe(dest("dist"));
}

function minimg() {
  return src("src/img/*").pipe(imagemin()).pipe(dest("dist/images"));
}

function serve() {
  sync.init({
    server: "./dist",
  });
  watch("src/**.html", series(html)).on("change", sync.reload);
  watch("src/scss/**.scss", series(scss)).on("change", sync.reload);
  watch("src/add.js", series(jsmin)).on("change", sync.reload);
}

exports.dev = series(clear, scss, html, jsmin, minimg, serve);
exports.build = series(clear, scss, html, jsmin, minimg);
