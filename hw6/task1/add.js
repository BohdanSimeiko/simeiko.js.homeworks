function createNewUser() {
  const newUser = {
    firstName: prompt("What is your name?"),
    lastName: prompt("What is your last name?"),
    birthday: prompt("Enter your birthday", "dd.mm.yyyy"),
    getLogin: function () {
      let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      return login;
    },
    getAge: function () {
      let userAge = this.birthday.slice(6.1);
      return 2022 - userAge;
    },
    getPassword: function () {
      let password =
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6.1);

      return password;
    },
  };
  return newUser;
}

const user = createNewUser();

console.log(user.getLogin());

console.log(user.getAge());

console.log(user.getPassword());
