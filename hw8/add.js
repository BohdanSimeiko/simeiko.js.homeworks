let paragraphs = document.getElementsByTagName("p");

for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

let elem = document.getElementById("optionsList");
console.log(elem);
console.log(elem.parentNode);
console.log(elem.childNodes);

document.getElementById("testParagraph").innerText = "This is a paragraph";

let header = document.getElementsByClassName("main-header")[0];
console.log(header.children);
for (let node of header.children) {
  node.classList.add("nav-item");
}

let title = document.getElementsByClassName("section-title");
for (let item of title) {
  item.classList.remowe("section-title");
}
// в этом HTML файле нету елементов с таким классом. Или я что-то не так понял...
