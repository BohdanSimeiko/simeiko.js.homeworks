const btns = document.querySelectorAll(".btn");
const btnNumOne = document.querySelector(".One");
const btnNumTwo = document.querySelector(".Two");
const btnNumThree = document.querySelector(".Three");
const btnNumFour = document.querySelector(".Four");
const btnNumFive = document.querySelector(".Five");
const btnNumSix = document.querySelector(".Six");
const btnNumSeven = document.querySelector(".Seven");

window.addEventListener("keydown", (event) => {
  if (
    btnNumOne.innerHTML === event.key ||
    btnNumOne.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumOne.classList.add("pressed");
  }
  if (
    btnNumTwo.innerHTML === event.key ||
    btnNumTwo.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumTwo.classList.add("pressed");
  }
  if (
    btnNumThree.innerHTML === event.key ||
    btnNumThree.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumThree.classList.add("pressed");
  }
  if (
    btnNumFour.innerHTML === event.key ||
    btnNumFour.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumFour.classList.add("pressed");
  }
  if (
    btnNumFive.innerHTML === event.key ||
    btnNumFive.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumFive.classList.add("pressed");
  }
  if (
    btnNumSix.innerHTML === event.key ||
    btnNumSix.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumSix.classList.add("pressed");
  }
  if (
    btnNumSeven.innerHTML === event.key ||
    btnNumSeven.innerHTML === event.key.toUpperCase()
  ) {
    btns.forEach((elem) => {
      elem.classList.remove("pressed");
    });
    btnNumSeven.classList.add("pressed");
  }
});
