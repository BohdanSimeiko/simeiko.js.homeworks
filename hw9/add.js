function createList(arr, parent = document.body) {
  const pageList = arr.map(function (elem) {
    return `<li>${elem}</li>`;
  });
  let list = document.createElement(`ul`);
  parent.prepend(list);
  list.innerHTML = pageList.join("");
}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", "Gdansk"]);
createList(["1", "2", "3", "sea", "user", 23]);
