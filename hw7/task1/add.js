let arr = ["hello", "world", 23, "23", null];

function filteredBy(array, typeOfItem) {
  const filteredArr = array.filter((item) => typeof item !== typeOfItem);
  return filteredArr;
}
console.log(filteredBy(arr, "string"));
