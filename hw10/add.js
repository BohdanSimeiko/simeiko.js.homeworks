const tabsList = document.querySelector(".tabs");

tabsList.addEventListener("click", (event) => {
  if (event.target.classList.contains("tabs-title")) {
    const targetsData = event.target.dataset.tab;
    document.querySelector(".tabs-title.active").classList.remove("active");
    event.target.classList.add("active");
    document
      .querySelector(".tab-content.active-tab")
      .classList.remove("active-tab");
    document
      .querySelector(`[data-content=${targetsData}-content]`)
      .classList.add("active-tab");
  }
});
