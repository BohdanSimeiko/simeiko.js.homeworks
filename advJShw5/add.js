const getUsersData = function () {
  Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((response) => {
      return response.json();
    }),
    fetch("https://ajax.test-danit.com/api/json/posts").then((response) => {
      return response.json();
    }),
  ]).then((responses) => {
    const users = responses[0];
    const posts = responses[1];
    createUserCard(users, posts);
  });
};

getUsersData();

const createUserCard = function (usersData, postsData) {
  const allUsers = usersData.map((element) => {
    const { id, name, email } = element;
    return { id, name, email };
  });
  const allPosts = postsData.map((element) => {
    const user = allUsers.find(({ id }) => {
      return id === element.userId;
    });
    return { ...user, ...element };
  });
  allPosts.forEach((elem) => {
    const { body, email, userId, name, title, id } = elem;
    const card = new Card(body, email, userId, name, title, id);
    card.render();
  });
};

class Card {
  constructor(body, email, userId, name, title, id) {
    this.title = title;
    this.userId = userId;
    this.body = body;
    this.name = name;
    this.email = email;
    this.id = id;
  }

  render() {
    const userCard = document.createElement("div");
    userCard.classList.add("card");
    userCard.insertAdjacentHTML("afterbegin", [
      `<h3>${this.title}</h3>
    <p>${this.body}</p>
    <p>${this.name}</p>
    <a href = mailto:${this.email}>${this.email}</a>
    <button id="deleteCard">X</button>`,
    ]);
    userCard.querySelector("#deleteCard").addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: "DELETE",
      }).then(() => {
        userCard.remove();
      });
    });
    document.body.append(userCard);
  }
}
