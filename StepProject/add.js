const serviceTabs = document.querySelector(".services-menu");

serviceTabs.addEventListener("click", (event) => {
  if (event.target.classList.contains("services-menu-item")) {
    const targetsData = event.target.dataset.tab;
    document
      .querySelector(".services-menu-item.active")
      .classList.remove("active");
    event.target.classList.add("active");
    document
      .querySelector(".servis-content.active-box")
      .classList.remove("active-box");
    document
      .querySelector(`[data-box=${targetsData}-content]`)
      .classList.add("active-box");
  }
});

const loadMoreBtn = document.querySelector(".work-btn");

loadMoreBtn.onclick = () => {
  imgContainer.forEach((elem) => {
    if (elem.classList.contains("invisible")) {
      elem.classList.remove("invisible");
    }
  });
  loadMoreBtn.style.display = "none";
};

const worksTab = document.querySelector(".work-nav-bar");
const imgContainer = document.querySelectorAll(".img-item");

worksTab.addEventListener("click", (event) => {
  if (event.target.tagName !== "LI") return false;
  let filterData = event.target.dataset.filter;

  imgContainer.forEach((elem) => {
    elem.classList.remove("hidden");
    if (!elem.classList.contains(filterData) && filterData !== "ALL") {
      elem.classList.add("hidden");
    }
  });
});

$(document).ready(function () {
  $(".slider").slick({
    dots: true,
    fade: true, //если это не считается как анимация карусели, можно поменять на false :D
  });
});
