const images = document.querySelectorAll(".image");
const pauseBtn = document.querySelector(".pause");
const playBtn = document.querySelector(".play");
let imgInterval = setInterval(nextImg, 3000);
let currentImg = 0;
let showing = true;

function nextImg() {
  images[currentImg].className = "image";
  currentImg = (currentImg + 1) % images.length;
  images[currentImg].className = "image show";
}

function pause() {
  showing = false;
  clearInterval(imgInterval);
}

function play() {
  showing = true;
  imgInterval = setInterval(nextImg, 3000);
}

pauseBtn.onclick = function () {
  if (showing) {
    pause();
    playBtn.classList.add("show");
  }
};

playBtn.onclick = function () {
  if (!showing) {
    play();
  }
};
