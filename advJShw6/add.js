const btn = document.createElement("button");
document.body.append(btn);
btn.innerText = "Вычислить по IP";
btn.addEventListener("click", findPersonsData);

async function findPersonsData() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const ip = await response.json();
  const responsAboutPers = await fetch(
    `http://ip-api.com/json/${ip.ip}?fields=continent,country,region,city,regionName`
  );
  const personsData = await responsAboutPers.json();
  showData(personsData);
}

function showData(data) {
  if (btn.nextElementSibling) btn.nextElementSibling.remove();
  const wraper = document.createElement("div");
  for (let param in data) {
    const paragraph = document.createElement("p");
    paragraph.innerText = `${param} – ${data[param]}`;
    wraper.append(paragraph);
  }
  btn.after(wraper);
}
