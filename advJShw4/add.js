const listWrap = document.querySelector("#films");
const films = document.createElement("ul");

listWrap.appendChild(films);

const filmsAPI = "https://ajax.test-danit.com/api/swapi/films";

function getFilms(url) {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open("GET", url);
    request.responseType = "json";
    request.send();
    request.onload = () => {
      if (request.status === 200) {
        resolve(request.response);
      } else {
        reject(request.statusText);
      }
    };
  });
}

function showFilms(url) {
  getFilms(url).then((data) => {
    data.forEach(({ episodeId, name, openingCrawl, characters }) => {
      const episodeNumber = document.createElement("h2");
      const filmDescr = document.createElement("p");
      const filmActors = document.createElement("ul");
      const actors = characters;
      films.appendChild(episodeNumber);
      films.append(filmActors);
      films.appendChild(filmDescr);
      episodeNumber.innerText = `Episodde number: ${episodeId}\n Name: ${name}\n`;
      filmDescr.innerText = `${openingCrawl}`;

      actors.forEach((actor) => {
        fetch(actor)
          .then((response) => response.json())
          .then((data) => {
            let actorName = document.createElement("li");
            filmActors.append(actorName);
            actorName.innerText = data.name;
          });
      });
    });
  });
}

showFilms(filmsAPI);
