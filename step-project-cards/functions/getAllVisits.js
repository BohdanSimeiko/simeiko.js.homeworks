import { getAllCards } from "./sendRequsts.js";
import { renderCardByClass } from "./renderCardByClass.js";
import { visitsArr } from "../add.js";
import { cardDesk } from "../add.js";

export function getAllVisits() {
  getAllCards().then((dataArr) => {
    if (dataArr.length == 0) {
      cardDesk.innerHTML = `<h2 class="no-items">No items have been added</h2>`;
    } else {
      dataArr.forEach((data) => {
        renderCardByClass(data);
        visitsArr.push(data);
      });
    }
  });
}
