export const createVisitBody = () => {
  const visitBody = document.createElement("div");
  visitBody.classList.add("create-vivsit-modal");
  visitBody.innerHTML = `
  <form id="visitForm" novalidate> 
  <select class="form-select doctor" name="doctor" aria-label="Default select example">
  <option selected disabled>Выберите врача.</option>
  <option value="Кардиолог">Кардиолог</option>
  <option value="Стоматолог">Стоматолог</option>
  <option value="Терапевт">Терапевт</option>
</select>
<div class="hidden" id="standartOptions">
<input type="text" class="form-control" name="purpose" placeholder="Цель визита." required>
<textarea class="form-control" name="description" placeholder="Краткое описание визита." id="floatingTextarea"></textarea>
<select class="form-select" name="urgency" aria-label="Default select example">
  <option selected disabled>Выберите срочность визита.</option>
  <option value="Неотложная">Неотложная</option>
  <option value="Приоритетная">Приоритетная</option>
  <option value="Обычная">Обычная</option>
</select>
<input type="text" class="form-control" name="name" placeholder="ФИО пациента." required>
<div class=" hidden" id="cardiologist">
<input type="number" class="form-control" name="normalPressure" placeholder="Нормальное давление пациента." required>
<input type="number" class="form-control" name="bodyMassIndex" placeholder="Индекс массы тела пациента." required>
<textarea class="form-control" name="pastDiseases" placeholder="Перенесенные ранее заболевания сердца." id="floatingTextarea"></textarea>
<input type="number" class="form-control" name="age" placeholder="Возраст пациента." required>
</div>
<div class=" hidden" id="dentist">
<input type="date" class="form-control" name="lastVisit" placeholder="Дата последнего посещения." required>
</div>
<div class=" hidden" id="therapist">
<input type="number" class="form-control" name="age" placeholder="Возраст пациента." required>
</div>
</div>
<button type="submit" id="save-btn" class="btn btn-success hidden">Создать визит</button>
</form>

 `;
  return visitBody.innerHTML;
};
