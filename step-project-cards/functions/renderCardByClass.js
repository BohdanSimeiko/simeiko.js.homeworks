import {
  VisitCardiologist,
  VisitDentist,
  VisitTherapist,
} from "../modules/visits.js";
import { cardDesk } from "../add.js";

export function renderCardByClass(obj) {
  if (obj.doctor === "Стоматолог") {
    const newCard = new VisitDentist(obj);
    cardDesk.append(newCard.renderСard());
  } else if (obj.doctor === "Кардиолог") {
    const newCard = new VisitCardiologist(obj);
    cardDesk.append(newCard.renderСard());
  } else if (obj.doctor === "Терапевт") {
    const newCard = new VisitTherapist(obj);
    cardDesk.append(newCard.renderСard());
  }
}
