export const API = "https://ajax.test-danit.com/api/v2/cards/";

// const password = "testPassword";
// const email = "testacc@test.com";

const sendRequest = async (API, entity, method = "GET", config) => {
  return await fetch(`${API}${entity}`, {
    method,
    ...config,
  }).then((response) => {
    if (response.ok) {
      if (method === "POST" && entity === "login") {
        return response.text();
      } else if (method === "GET" || method === "POST" || method === "PUT") {
        return response.json();
      }
      return response;
    }
  });
};

const newUserLogin = (user) =>
  sendRequest(API, "login", "POST", {
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(user),
  });

const createCard = (cardData) =>
  sendRequest(API, "", "POST", {
    headers: {
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token")
        ? `Bearer ${localStorage.getItem("token")}`
        : undefined,
    },
    body: JSON.stringify(cardData),
  });

const deleteCard = (cardId) =>
  sendRequest(API, `${cardId}`, "DELETE", {
    headers: {
      Authorization: localStorage.getItem("token")
        ? `Bearer ${localStorage.getItem("token")}`
        : undefined,
    },
  });

const getAllCards = () =>
  sendRequest(API, "", "GET", {
    headers: {
      Authorization: localStorage.getItem("token")
        ? `Bearer ${localStorage.getItem("token")}`
        : undefined,
    },
  });

const getOneCard = (cardId) =>
  sendRequest(API, `${cardId}`, "GET", {
    headers: {
      Authorization: localStorage.getItem("token")
        ? `Bearer ${localStorage.getItem("token")}`
        : undefined,
    },
  });

export { createCard, deleteCard, getAllCards, getOneCard, newUserLogin };
