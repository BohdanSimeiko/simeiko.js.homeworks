export const createVisitObject = (form) => {
  let visitObject = {};
  for (let [name, value] of form) {
    if (value) {
      visitObject[name] = value;
    } else if (!value) {
      visitObject[name] = "Данные отсутствуют";
    }
  }
  return visitObject;
};
