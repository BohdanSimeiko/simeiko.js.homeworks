export const createLogInBody = () => {
  const logInBody = document.createElement("div");
  logInBody.innerHTML = `
    <form id='logInForm'>
  <input type="text" class="form-control" name="login" id="login" placeholder="Введите логин." required>
  <input type="password" class="form-control" name="password"  id="password" placeholder="Введите пароль." required>
  <span class="no-valid hidden" id="noValid">Введен неправильный логин или пароль, попробуйте еще раз.</span>
  <br><button type="submit" id="login-btn" class="btn btn-success">Войти</button>
</form>
    `;
  return logInBody.innerHTML;
};
