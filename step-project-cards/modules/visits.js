export class Visit {
  constructor({ id, doctor, purpose, description, urgency, name }) {
    this.id = id;
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
  }

  renderСard() {
    this.visitCard = document.createElement("div");
    this.visitCard.classList.add("card");
    this.visitCard.dataset.id = this.id;

    this.visitCard.insertAdjacentHTML(
      "afterBegin",
      `
          <div class="card-body">
            <h6 class="card-title"> Пациент: ${this.name}</h6>
            <div class="edit" id="edit" >edit</div>
            <div class="save hidden" id="save" >save</div>
            <button type="button" class="btn-close" id="closeBtn"></button>
            <p class="card-text">
              Доктор: ${this.doctor}
            </p>
            <div class="visit-info hidden">
                <ul> 
                    <li>Цель визита: ${this.purpose}</li>
                    <li>Краткое описание визита: ${this.description}</li>
                    <li>Срочность: ${this.urgency}</li>
                
                    ${
                      this.normalPressure
                        ? `<li>Давление: ${this.normalPressure}</li>`
                        : ""
                    }
                    ${this.age ? `<li>Возраст: ${this.age}</li>` : ""}
                    ${
                      this.pastDiseases
                        ? `<li>Перенесенные заболевания: ${this.pastDiseases}</li>`
                        : ""
                    }
                    ${
                      this.bodyMassIndex
                        ? `<li>Индекс массы тела: ${this.bodyMassIndex}</li>`
                        : ""
                    }
                    ${
                      this.lastVisit
                        ? `<li>Дата последнего посещения: ${this.lastVisit}</li>`
                        : ""
                    }
                </ul>
            </div>
            <button class="btn btn-outline-success card-btn" id="toggleBtn" type="button">
               Развернуть
             </button>
          </div>
        `
    );
    return this.visitCard;
  }
}

export class VisitDentist extends Visit {
  constructor({ id, doctor, purpose, description, urgency, name, lastVisit }) {
    super({ id, doctor, purpose, description, urgency, name });
    this.lastVisit = lastVisit;
  }

  renderСard() {
    super.renderСard();
    return this.visitCard;
  }
}

export class VisitTherapist extends Visit {
  constructor({ id, doctor, purpose, description, urgency, name, age }) {
    super({ id, doctor, purpose, description, urgency, name });
    this.age = age;
  }

  renderСard() {
    super.renderСard();
    return this.visitCard;
  }
}

export class VisitCardiologist extends Visit {
  constructor({
    id,
    doctor,
    purpose,
    description,
    urgency,
    name,
    age,
    pastDiseases,
    bodyMassIndex,
    normalPressure,
  }) {
    super({ id, doctor, purpose, description, urgency, name });
    this.age = age;
    this.pastDiseases = pastDiseases;
    this.bodyMassIndex = bodyMassIndex;
    this.normalPressure = normalPressure;
  }

  renderСard() {
    super.renderСard();
    return this.visitCard;
  }
}
