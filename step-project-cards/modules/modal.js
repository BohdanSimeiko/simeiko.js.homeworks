export default class Modal {
  constructor({ headerTitle, body, closeOutside = false }) {
    this.headerTitle = headerTitle;
    this.body = body;
    this.closeOutside = closeOutside;
  }

  attachListener() {
    document.body.addEventListener("click", (e) => {
      let modal = e.target.classList.contains("modal");
      let close = e.target.classList.contains("btn-close");
      if ((modal && this.closeOutside) || close) {
        this.close();
      }
    });
  }

  close() {
    this.backGround.remove();
    document.body.classList.remove("modal-open");
    this.modal.remove();
  }

  renderBackground() {
    this.backGround = document.createElement("div");
    this.backGround.classList.add("modal-backdrop");
    document.body.append(this.backGround);
  }

  render() {
    this.modal = document.createElement("div");
    this.modal.classList.add("modal");
    this.modal.style.display = "block";
    document.body.classList.add("modal-open");
    this.modal.innerHTML = `
            <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title">${this.headerTitle}</h5>
                        <button type="button" class="btn-close" aria-label="Close"></button>
                     </div>
                    <div class="modal-body">
                        ${this.body ? this.body : ""}
                    </div>
                </div>
            </div>
        `;

    this.renderBackground();
    this.attachListener();
    return this.modal;
  }
}
