import Modal from "./modules/modal.js";
import { createVisitBody } from "./functions/createVisitModalBody.js";
import { createVisitObject } from "./functions/createVisitObj.js";
import {
  createCard,
  deleteCard,
  newUserLogin,
} from "./functions/sendRequsts.js";
import { getAllVisits } from "./functions/getAllVisits.js";
import { renderCardByClass } from "./functions/renderCardByClass.js";
import { createLogInBody } from "./functions/createLogInModalBody.js";

const btnLogOut = document.querySelector("#logOut");
const btnLogIn = document.querySelector("#logIn");
const btnOpenModal = document.querySelector("#open-modal");
export const cardDesk = document.querySelector("#cards-desc");
export const visitsArr = [];

window.addEventListener("DOMContentLoaded", () => {
  const token = localStorage.getItem("token");
  if (token) {
    btnLogIn.classList.add("hidden");
    btnLogOut.classList.remove("hidden");
    btnOpenModal.classList.remove("hidden");
    getAllVisits();
  } else {
    cardDesk.innerHTML = `<h3 class="no-token-info">Для работы на сайте нужно войти в свой аккант.</h3>`;
  }
});

btnLogIn.addEventListener("click", () => {
  const loginModal = new Modal({
    headerTitle: "Авторизация пользователя.",
    body: createLogInBody(),
    closeOutside: true,
  });
  document.body.append(loginModal.render());

  const login = document.querySelector("#login");
  const password = document.querySelector("#password");
  const loginForm = document.querySelector("#logInForm");
  const noValid = document.querySelector("#noValid");

  loginForm.addEventListener("submit", (event) => {
    event.preventDefault();

    newUserLogin({
      email: login.value,
      password: password.value,
    }).then((token) => {
      if (token) {
        cardDesk.innerHTML = "";
        localStorage.setItem("token", `${token}`);
        getAllVisits();
        loginModal.close();
        btnLogIn.classList.add("hidden");
        btnLogOut.classList.remove("hidden");
        btnOpenModal.classList.remove("hidden");
      } else {
        noValid.classList.remove("hidden");
      }
    });
  });
});

btnOpenModal.addEventListener("click", () => {
  const visitModal = new Modal({
    headerTitle: "Создание нового визита.",
    body: createVisitBody(),
    closeOutside: true,
  });
  document.body.append(visitModal.render());

  const visitForm = document.querySelector("#visitForm");
  const doctor = document.querySelector(".doctor");
  const saveBtn = document.querySelector("#save-btn");
  const allDoctorOptions = document.querySelector("#standartOptions");
  const cardiologist = document.querySelector("#cardiologist");
  const dentist = document.querySelector("#dentist");
  const therapist = document.querySelector("#therapist");

  doctor.addEventListener("change", () => {
    if (doctor.value === "Кардиолог") {
      allDoctorOptions.classList.remove("hidden");
      cardiologist.classList.remove("hidden");
      dentist.classList.add("hidden");
      therapist.classList.add("hidden");
    } else if (doctor.value === "Стоматолог") {
      allDoctorOptions.classList.remove("hidden");
      dentist.classList.remove("hidden");
      cardiologist.classList.add("hidden");
      therapist.classList.add("hidden");
    } else if (doctor.value === "Терапевт") {
      allDoctorOptions.classList.remove("hidden");
      therapist.classList.remove("hidden");
      cardiologist.classList.add("hidden");
      dentist.classList.add("hidden");
    }
    saveBtn.classList.remove("hidden");
  });

  visitForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(visitForm);
    let cardData = createVisitObject(formData);
    visitModal.close();
    if (
      cardDesk.innerHTML ===
      `<h2 class="no-items">No items have been added</h2>`
    ) {
      cardDesk.innerHTML = "";
    }
    await createCard(cardData).then((visitCard) => {
      renderCardByClass(visitCard);
    });
  });
});

document.addEventListener("click", async (event) => {
  if (event.target.id === "closeBtn") {
    const visitCard = event.target.closest(".card");
    const cardId = visitCard.dataset.id;
    await deleteCard(cardId).then((res) => {
      if (res.ok) {
        visitCard.remove();
        if (cardDesk.innerHTML === "") {
          cardDesk.innerHTML = `<h2 class="no-items">No items have been added</h2>`;
        }
      }
    });
  } else if (event.target.id === "toggleBtn") {
    const moreInfo = event.target.previousElementSibling;
    moreInfo.classList.toggle("hidden");
    if (event.target.innerText === "Развернуть") {
      event.target.innerText = "Свернуть";
    } else {
      event.target.innerText = "Развернуть";
    }
  }
});

btnLogOut.addEventListener("click", () => {
  localStorage.clear();
  location.reload();
});
