const passwordIcon = document.querySelector(".icon-password");
const confirmPassIcon = document.querySelector(".icon-password-confirm");
const confirm = document.querySelector(".btn");
const showPasswordBot = document.getElementById("secondInput");
const showPasswordTop = document.getElementById("firstInput");
const error = document.querySelector(".error");

passwordIcon.addEventListener("click", function () {
  if (showPasswordTop.type === "password") {
    passwordIcon.classList.remove("fa-eye");
    passwordIcon.classList.add("fa-eye-slash");
    showPasswordTop.type = "text";
  } else {
    passwordIcon.classList.remove("fa-eye-slash");
    passwordIcon.classList.add("fa-eye");
    showPasswordTop.type = "password";
  }
});

confirmPassIcon.addEventListener("click", function () {
  if (showPasswordBot.type === "password") {
    confirmPassIcon.classList.remove("fa-eye");
    confirmPassIcon.classList.add("fa-eye-slash");
    showPasswordBot.type = "text";
  } else {
    confirmPassIcon.classList.remove("fa-eye-slash");
    confirmPassIcon.classList.add("fa-eye");
    showPasswordBot.type = "password";
  }
});

confirm.addEventListener("click", function (event) {
  event.preventDefault();
  if (showPasswordTop.value === showPasswordBot.value) {
    if (error.classList.contains("show")) {
      error.classList.remove("show");
    }
    alert("You are welcome!");
  } else {
    error.classList.add("show");
  }
});
