const wraper = document.getElementById("root");
const ul = document.createElement("ul");
wraper.appendChild(ul);

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const keys = ["author", "name", "price"];

function booksFilter(arr, objKeys) {
  arr.forEach((item, index) => {
    try {
      objKeys.forEach((key) => {
        if (!(key in item))
          throw new Error(`Отсутсвует ${key} в книге №${index + 1}`);
      });
      const li = document.createElement("li");
      li.innerText = `Название книги: "${item.name}"\n Автор: ${item.author}\n Цена: ${item.price}`;
      ul.appendChild(li);
    } catch (error) {
      console.log(error.message);
    }
  });
}

booksFilter(books, keys);
