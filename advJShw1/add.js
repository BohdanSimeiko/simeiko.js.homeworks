class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    return `${this.name}`;
  }
  getAge() {
    return this.age;
  }
  getSalary() {
    return this.salary;
  }
  setName(value) {
    this.name = value;
  }
  setAge(value) {
    this.age = value;
  }
  setSalary(value) {
    this.salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  getSalary() {
    return this.salary * 3;
  }
}

const programer1 = new Programmer("Alex", 27, 3400, ["ua", "eng", "ru"]);
console.log(programer1);

const programer2 = new Programmer("Bogdan", 24, 5100, [
  "ua",
  "eng",
  "ru",
  "pl",
]);
console.log(programer2);
